FROM node:17-alpine

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY server.js .

EXPOSE 5001
CMD ["npm", "start"]